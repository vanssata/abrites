

/* eslint-env browser */

import axios from 'axios';
import serialize from 'form-serialize';

const SyliusAddToCart = (el) => {
  const element = el;
  const url = element.getAttribute('action');
  const redirectUrl = element.getAttribute('data-redirect');
  const validationElement = element.querySelector('[data-js-add-to-cart="error"]');

  element.addEventListener('submit', (e) => {
    const request = axios.post(url, serialize(element));

    e.preventDefault();

    request.then(() => {
      validationElement.classList.add('d-none');
      window.location.reload();
    });

    request.catch((error) => {
      validationElement.classList.remove('d-none');
      let validationMessage = '';

      Object.entries(error.response.data).forEach(([, message]) => {
        validationMessage += message;
      });

      validationElement.innerHTML = validationMessage;
      this.element.classList.remove('loading');
    });
  });
};

export default SyliusAddToCart;
