// Main scripts file
require("slick-carousel/slick/slick.css");
require("slick-carousel/slick/slick-theme.css");

import './js/index';

// Main styles file
import './scss/index.scss';

// Images
import './media/sylius-logo.png';
import './media/logo-small.svg';

// Font awesome icons
import './js/fontawesome';

import '@fortawesome/fontawesome-free/css/all.min.css';
import '@fortawesome/fontawesome-free/js/all.js';
const $ = require('jquery');
import './js/modal';

import 'bootstrap/js/dist/modal';

require("slick-carousel");


$(function () {
  var breakpoint = {
    // Small screen / phone
    sm: 576,
    // Medium screen / tablet
    md: 768,
    // Large screen / desktop
    lg: 992,
    // Extra large screen / wide desktop
    xl: 1200
  };

  var $slider =   $('.slick-slider').on('init', function(slick) {

    $('.slick-slider').show();
  }).slick({
    autoplay: false,
    draggable: true,
    infinite: false,
    dots: false,
    arrows: true,
    speed: 1000,
    mobileFirst: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    lazyLoad: 'ondemand',
    responsive: [
      {
        breakpoint: breakpoint.sm,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: breakpoint.md,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3
        }
      },
      {
        breakpoint: breakpoint.lg,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 4
        }
      },
      {
        breakpoint: breakpoint.xl,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 4
        }
      }
    ]
  });
});
$(function () {
  $('.js-add-to-cart').on('click',function (e) {
      e.preventDefault();
      var url = $(e.currentTarget).attr('href');

      $.get(url,function (data) {
        $.fn.dialogue({
          title: "Product View",
          content: $("<div/>").html(data),
          buttons: [
            { text: "Close", id: $.utils.createUUID(), click: function ($modal) { $modal.dismiss(); } }
          ]
        });
      });
  });
});


window.addEventListener('load', function(){
  var allimages= document.getElementsByTagName('img');
  for (var i=0; i<allimages.length; i++) {
    if (allimages[i].getAttribute('data-src')) {
      allimages[i].setAttribute('src', allimages[i].getAttribute('data-src'));
    }
  }
}, false)
