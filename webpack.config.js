const Encore = require('@symfony/webpack-encore');
const bootstrapTheme = require('./themes/BootstrapTheme/webpack.config');
module.exports = [bootstrapTheme];
