System Requirements
------------
[requirements for running Symfony](https://symfony.com/doc/current/reference/requirements.html)

Operating Systems
-----------------   
The recommended operating systems for running this implementations are the Unix systems - Linux or MacOS.

required modules and configuration
--------------------------------------
>### PHP Extesnions: 
> - gd
> - exif
> - fileinfo
> - intl
> - sqlite3

> ### JS Extesnions: 
> - node10 
> - yarn

# Admin Access: 
---------------
u: sylius  
p: sylius  
h: /admin  


Installation whit GIT
------------
```bash 
mkdir abrites
cd abrites
git clone https://vanssata@bitbucket.org/vanssata/abrites.git .
wget http://getcomposer.org/composer.phar
php composer.phar install
#Build Default Theme
yarn install 
yarn build 
#Build Custom theme 
yarn encore dev 
php bin/console server:start
./bin/console assets:install
./bin/console sylius:theme:assets:install
```
