<?php
declare(strict_types=1);


namespace App\Form\Extension;

use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Sylius\Bundle\ProductBundle\Form\Type\ProductTranslationType;

final class ProductTranslationTypeExtension extends AbstractTypeExtension
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('is_featured', CheckboxType::class, [
                'required' => false,
                'label' => 'Featured',
            ])
            ->add('is_most_wanded', CheckboxType::class, [
                'required' => false,
                'label' => 'Most Wanded',
            ])
            ->add('is_most_popular', CheckboxType::class, [
                'required' => false,
                'label' => 'Most Popular',
            ]);

    }
    public function getParent()
    {
        return ProductTranslationType::class;
    }

    public function getExtendedTypes(): iterable{
        return [ProductTranslationType::class];
    }

}
