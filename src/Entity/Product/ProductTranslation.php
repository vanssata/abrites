<?php

declare(strict_types=1);

namespace App\Entity\Product;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\MappedSuperclass;
use Doctrine\ORM\Mapping\Table;
use Sylius\Component\Core\Model\ProductTranslation as BaseProductTranslation;

/**
 * @MappedSuperclass
 * @Table(name="sylius_product_translation")
 */
class ProductTranslation extends BaseProductTranslation
{

    /**
     * @ORM\Column(type="boolean", name="featured", nullable=true )
     */
    private $is_featured;
    /**
     * @ORM\Column(type="boolean", name="most_wanded", nullable=true)
     */
    private $is_most_wanded;
    /**
     * @ORM\Column(type="boolean",name="most_popular", nullable=true)
     */
    private $is_most_popular;

    /**
     * @return mixed
     */
    public function getIsFeatured()
    {
        return $this->is_featured;
    }

    /**
     * @param mixed $is_featured
     * @return ProductTranslation
     */
    public function setIsFeatured($is_featured)
    {
        $this->is_featured = $is_featured;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsMostWanded()
    {
        return $this->is_most_wanded;
    }

    /**
     * @param mixed $is_most_wanded
     * @return ProductTranslation
     */
    public function setIsMostWanded($is_most_wanded)
    {
        $this->is_most_wanded = $is_most_wanded;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsMostPopular()
    {
        return $this->is_most_popular;
    }

    /**
     * @param mixed $is_most_popular
     * @return ProductTranslation
     */
    public function setIsMostPopular($is_most_popular)
    {
        $this->is_most_popular = $is_most_popular;
        return $this;
    }



}
