<?php


namespace App\Repository;
use App\Entity\Product\ProductAttribute;
use Doctrine\ORM\QueryBuilder;
use Sylius\Bundle\CoreBundle\Doctrine\ORM\ProductRepository as BaseProductRepository;
use Sylius\Component\Core\Model\ChannelInterface;
use Doctrine\ORM\Query\Expr\Join;

class ProductRepository  extends BaseProductRepository
{
    /**
     * @param ChannelInterface $channel
     * @param string $locale
     * @param string $type
     * @param $count
     * @return mixed
     */
    public function findProductsByTagType(ChannelInterface $channel, string $locale, $type='is_featured', $count)
    {
       return  $this->findAttributeByChannel($channel,$locale,$count,'is_'.$type)->getQuery()->getResult();
    }

    public function findAttributeByChannel(ChannelInterface $channel,string $locale, int $count, string $attributeCode): QueryBuilder
    {
        return $this->createQueryBuilder('o')
            ->innerJoin('o.attributes', 'attribute')
            ->innerJoin('o.translations', 'translation', 'WITH', 'translation.locale = :locale')
            ->andWhere(':channel MEMBER OF o.channels')
            ->andWhere("translation.$attributeCode = true")
            ->setParameter('locale', $locale)
            ->andWhere('o.enabled = true')
            ->addOrderBy('o.createdAt', 'DESC')
            ->setParameter('channel', $channel)
            ->setMaxResults($count)
            ;
    }

}
